window.onload = () => {
  document.querySelector('#sidebar button').addEventListener('click', e => {
    document.querySelector('#sidebar').classList.toggle('collapsed')
    e.currentTarget.setAttribute(
      'aria-expanded',
      e.currentTarget.getAttribute('aria-expanded') === 'false',
    )
  })

  const collapseMobileTOC = () => {
    if (window.innerWidth < 800) {
      document.querySelector('#sidebar').classList.add('collapsed')
      document
        .querySelector('#sidebar button')
        .setAttribute('aria-expanded', 'false')
    }
  }

  collapseMobileTOC()

  window.addEventListener('resize', collapseMobileTOC)
}
